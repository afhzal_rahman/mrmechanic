//
//  MechanicPersistenceTests.swift
//  MrMechanic
//
//  Created by Fadzlil on 12/2/15.
//  Copyright © 2015 ACME. All rights reserved.
//

import XCTest
@testable import MrMechanic

class MechanicPersistenceTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testWritingToFile() {
        let fileManager: NSFileManager = NSFileManager.defaultManager()
        let documentsDirectory: NSURL = fileManager.URLsForDirectory(NSSearchPathDirectory.DocumentDirectory, inDomains: NSSearchPathDomainMask.UserDomainMask).first!
        
        let filePath: NSURL = documentsDirectory.URLByAppendingPathComponent("data.plist")
        
        var mechanics: [Mechanic] = []
        mechanics.append(Mechanic(name: "Alibaba Workshop", address: "125, Jalan Kolam Ayer Lama",contactNo: "012-345-6789")!)
        mechanics.append(Mechanic(name: "AAA", address: "125, Jalan Kolam Ayer Lama",contactNo: "012-345-6789")!)
        mechanics.append(Mechanic(name: "BBB", address: "125, Jalan Kolam Ayer Lama",contactNo: "012-345-6789")!)
        
        var mechanicsData: [NSData] = []
        for mechanic in mechanics {
            let data: NSData = NSKeyedArchiver.archivedDataWithRootObject(mechanic)
            mechanicsData.append(data)
        }
        
        (mechanicsData as NSArray).writeToURL(filePath, atomically: true)
        
        var fileExists: Bool = fileManager.fileExistsAtPath(filePath.path!)
        XCTAssert(fileExists, "File should exist")
        
        NSLog("File path: \(filePath.path!)")
    }
    
    func testReadingFromFile() {
        let fileManager: NSFileManager = NSFileManager.defaultManager()
        let documentsDirectory: NSURL = fileManager.URLsForDirectory(NSSearchPathDirectory.DocumentDirectory, inDomains: NSSearchPathDomainMask.UserDomainMask).first!
        let filePath: NSURL = documentsDirectory.URLByAppendingPathComponent("data.plist")
        
        let mechanicData: [NSData] = NSArray(contentsOfURL: filePath) as! [NSData]
        var mechanics: [Mechanic] = []
        
        for data in mechanicData {
            let mechanic: Mechanic = NSKeyedUnarchiver.unarchiveObjectWithData(data) as! Mechanic
            mechanics.append(mechanic)
        }
        
        XCTAssert(mechanics.count == 3, "Should have loaded 3 parks from file")
    }
    
    func testSavingMechanic() {
        let defaults: NSUserDefaults = NSUserDefaults.standardUserDefaults()
        
        let mechanic = Mechanic(name: "Alibaba Workshop", address: "125, Jalan Kolam Ayer Lama",contactNo: "012-345-6789")
        let mechanicData: NSData = NSKeyedArchiver.archivedDataWithRootObject(mechanic!)
        
        defaults.setObject(mechanicData, forKey: "test")
        defaults.synchronize()
        
        let readMechanicData: NSData = defaults.objectForKey("test") as! NSData
        let readMechanic: Mechanic? = NSKeyedUnarchiver.unarchiveObjectWithData(readMechanicData) as? Mechanic
        XCTAssertNotNil(readMechanic, "Read back from saved data")
        XCTAssertEqual(readMechanic!.name, mechanic!.name, "Names should match")
    }
}
