//
//  MechanicModelTests.swift
//  MrMechanic
//
//  Created by Fadzlil on 12/2/15.
//  Copyright © 2015 ACME. All rights reserved.
//

import XCTest
import UIKit

@testable import MrMechanic

class MechanicModelTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testMechanicInitialization() {
        let testMechanic: Mechanic = Mechanic(name: "The Battery", address: "128, Jalan Medang", contactNo: "012-2345234")!
        XCTAssertNotNil(testMechanic, "Test mechanic should not be nil")
        
        let unnamedMechanic: Mechanic? = Mechanic(name: "", address: "128, Jalan Medang", contactNo: "012-2345234")
        XCTAssertNil(unnamedMechanic, "Mechanic cannot be created without a name")
    }
    
}
