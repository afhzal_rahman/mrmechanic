//
//  CarEmergencyListViewController.swift
//  MrMechanic
//
//  Created by Afhzal Abdul Rahman on 11/26/15.
//  Copyright © 2015 ACME. All rights reserved.
//

import UIKit

class CarEmergencyListViewController: UIViewController {

    @IBOutlet weak var CarEmergenciesTableView: UITableView!
    @IBOutlet weak var NextButton: UIButton!
    
    var emergencies: [String] = ["Car won't start","Got a flat tire","Engine Overheated","Ran out of fuel", "Got into an accident"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}

//implement extension for DataSource
extension CarEmergencyListViewController: UITableViewDataSource {
    
    //return number of rows
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return emergencies.count
    }
    
    //return cell selected
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: UITableViewCell = UITableViewCell(style: .Subtitle, reuseIdentifier: "Cell")
        
        let emergency: String = self.emergencies[indexPath.row]
        
        //print("Row selected is \(indexPath.row)")
        
        cell.textLabel?.text = emergency
        
        return cell
    }
    
}

//implement extension for Delegate
extension CarEmergencyListViewController : UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

        let mechanicsVC: MechanicsViewController = self.storyboard?.instantiateViewControllerWithIdentifier("MechanicsViewController") as! MechanicsViewController
        
        self.navigationController?.pushViewController(mechanicsVC, animated: true)
        
        //TODO: pass category to the MechanicsViewController 
        
        print("User selected row \(indexPath.row)")
    }
}

