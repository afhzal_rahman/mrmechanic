//
//  MechanicCell.swift
//  MrMechanic
//
//  Created by Afhzal Abdul Rahman on 11/27/15.
//  Copyright © 2015 ACME. All rights reserved.
//

import UIKit

class MechanicCell: UITableViewCell {
    
    @IBOutlet weak var mechanicLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var contactNoLabel: UILabel!
    @IBOutlet weak var logoImageView: UIImageView!
    
    var mechanic: Mechanic! {
        didSet {
            updateDisplay()
        }
    }
    
    func updateDisplay() {
        self.mechanicLabel.text = self.mechanic.name
        self.addressLabel.text = self.mechanic.address
        self.contactNoLabel.text = self.mechanic.contactNo
        self.logoImageView.image = self.mechanic.logo
    }
}

