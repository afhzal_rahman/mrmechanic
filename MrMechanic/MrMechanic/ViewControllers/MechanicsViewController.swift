//
//  MechanicsViewController.swift
//  MrMechanic
//
//  Created by Afhzal Abdul Rahman on 11/26/15.
//  Copyright © 2015 ACME. All rights reserved.
//

import UIKit
import CoreLocation

class MechanicsViewController: UIViewController {
        
    @IBOutlet weak var mechanicsTableView: UITableView!
    
    var mechanics: [Mechanic] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        preload()
    }
    
    func preload() {
        
        let mechanic1 = Mechanic(name: "Alibaba Workshop", address: "125, Jalan Kolam Ayer Lama",contactNo: "012-345-6789")
        let mechanic2 = Mechanic(name: "Best Denko Service", address: "111, Jalan Satu",contactNo: "012-345-1234")
        let mechanic3 = Mechanic(name: "Daniel's Tow Service", address: "123, Jalan-jalan Cari Makan",contactNo: "012-345-5454")
        let mechanic4 = Mechanic(name: "AAM Breakdown Assist", address: "1, Jalan Americas",contactNo: "012-345-4004")
        let mechanic5 = Mechanic(name: "Kurnia Motors", address: "1, Jalan Kurnia",contactNo: "012-345-3838")
        let mechanic6 = Mechanic(name: "BMW Excellent Service", address: "125, Jalan Kolam Ayer Lama",contactNo: "012-345-6789")
        let mechanic7 = Mechanic(name: "Maybank Best Service In Town", address: "111, Jalan Satu",contactNo: "012-345-1234")
        let mechanic8 = Mechanic(name: "I Help You Help Me Service", address: "123, Jalan-jalan Cari Makan",contactNo: "012-345-5454")
        let mechanic9 = Mechanic(name: "Just Another Workshop", address: "1, Jalan Americas",contactNo: "012-345-4004")
        let mechanic10 = Mechanic(name: "Your Reliable Mechanic", address: "1, Jalan Kurnia",contactNo: "012-345-3838")

        self.mechanics.append(mechanic1!)
        self.mechanics.append(mechanic2!)
        self.mechanics.append(mechanic3!)
        self.mechanics.append(mechanic4!)
        self.mechanics.append(mechanic5!)
        self.mechanics.append(mechanic6!)
        self.mechanics.append(mechanic7!)
        self.mechanics.append(mechanic8!)
        self.mechanics.append(mechanic9!)
        self.mechanics.append(mechanic10!)
        
        self.mechanicsTableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//implement extension for DataSource
extension MechanicsViewController: UITableViewDataSource {
    
    //return number of rows
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mechanics.count
    }
    
    //return cell selected
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

        //standard, inefficient way to display
        //let cell: UITableViewCell = UITableViewCell(style: .Subtitle, reuseIdentifier: "Cell")
        
        //dequeue method of displaying cells
        let cell: MechanicCell = self.mechanicsTableView.dequeueReusableCellWithIdentifier("MechanicCell", forIndexPath: indexPath) as! MechanicCell
        
        let mechanic: Mechanic = self.mechanics[indexPath.row]
        //cell.mechanic = mechanic
        
        //comment due to no longer required after using custom tableview cell
        cell.textLabel?.text = mechanic.name
        cell.textLabel?.text = mechanic.address
        cell.imageView?.image = mechanic.logo
        
        return cell
    }
    
}

//implement extension for Delegate
extension MechanicsViewController : UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        print("User selected row \(indexPath.row)")
    }
}

