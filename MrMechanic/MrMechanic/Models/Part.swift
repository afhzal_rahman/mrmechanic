//
//  Part.swift
//  MrMechanic
//
//  Created by Afhzal Abdul Rahman on 11/18/15.
//  Copyright © 2015 ACME. All rights reserved.
//

import Foundation

class Part {
    var name: String
    var type: String
    var availableQty: Int
    var partID: String
    
    init(name: String, type: String, availableQty: Int, partID: String) {
        self.name = name
        self.type = type
        self.availableQty = availableQty
        self.partID = partID
    }
    
}
