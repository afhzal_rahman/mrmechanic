//
//  Vehicle.swift
//  MrMechanic
//
//  Created by Fadzlil on 11/18/15.
//  Copyright © 2015 ACME. All rights reserved.
//

import Foundation

class Vehicle {
    
    var model: String
    var manufacturer: String
    var capacity: Int
    var year: Int
    var tyreSize: String
    
    init(model: String, manufacturer: String, capacity: Int, year: Int, tyreSize: String) {
        self.model = model
        self.manufacturer = manufacturer
        self.capacity = capacity
        self.year = year
        self.tyreSize = tyreSize
    }
}