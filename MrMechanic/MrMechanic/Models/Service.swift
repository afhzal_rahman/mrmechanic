//
//  Service.swift
//  MrMechanic
//
//  Created by Afhzal Abdul Rahman on 11/18/15.
//  Copyright © 2015 ACME. All rights reserved.
//

import Foundation

class Service {
    var name: String
    var serviceType: String
    var price: Double
    
    init(name: String, serviceType: String, price: Double) {
        self.name = name
        self.serviceType = serviceType
        self.price = price
    }
    
}
