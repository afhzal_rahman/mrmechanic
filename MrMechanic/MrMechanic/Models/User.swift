//
//  User.swift
//  MrMechanic
//
//  Created by Fadzlil on 11/18/15.
//  Copyright © 2015 ACME. All rights reserved.
//

import Foundation
import CoreLocation

class User {
    
    var name: String
    var location: CLLocation
    var contactNumber: String
    var vehicles: [Vehicle]
    var issues: [Issue]
    
    init(name: String, location: CLLocation, contactNumber: String, vehicles: [Vehicle], issues: [Issue]) {
        self.name = name
        self.location = location
        self.contactNumber = contactNumber
        self.vehicles = vehicles
        self.issues = issues
    }
}