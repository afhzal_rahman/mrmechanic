//
//  Emergency.swift
//  MrMechanic
//
//  Created by Afhzal Abdul Rahman on 11/25/15.
//  Copyright © 2015 ACME. All rights reserved.
//

import Foundation

class Emergency {
    var id: String
    var name: String
    var description: String
    
    init(id: String, name: String, description: String) {
        self.id = id
        self.name = name
        self.description = description
    }
    
}