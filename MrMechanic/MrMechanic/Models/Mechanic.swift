//
//  Mechanic.swift
//  MrMechanic
//
//  Created by Afhzal Abdul Rahman on 11/18/15.
//  Copyright © 2015 ACME. All rights reserved.
//

import Foundation
import CoreLocation
import UIKit

class Mechanic: NSObject, NSCoding {
    var name: String
    var address: String?
    var contactNo: String?
    var logo: UIImage?
    
    //skip location for now as don't know how to handle CLLocation
    //var location: CLLocation

    // do later if got time
    //var services: [Service]
    //var operatingHour: [OperatingHour]
    //var parts: [Part]
    
    //init?(name: String) {
    //    self.name = name
    //    super.init()
    //}
    
    init?(name: String, address: String, contactNo: String){
        
        self.name = name
        self.address = address
        self.contactNo = contactNo
        self.logo = UIImage(named: "AAM_logo")
        //self.location = location
        //self.services = services
        //self.operatingHour = operatingHour
        //self.parts = parts

        super.init()
        
        if name.isEmpty {
            return nil
        }
    }
    
    func encodeWithCoder(aCoder: NSCoder) {
        let keyedArchiver: NSKeyedArchiver = aCoder as! NSKeyedArchiver
        
        keyedArchiver.encodeObject(self.name, forKey: "MechName")
        keyedArchiver.encodeObject(self.address, forKey: "MechAddress")
        keyedArchiver.encodeObject(self.contactNo, forKey: "MechContactNo")
        
        if self.logo != nil {
            let imageData = UIImagePNGRepresentation(self.logo!)
            keyedArchiver.encodeObject(imageData, forKey: "MechLogo")
        }
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        let keyedUnarchiver: NSKeyedUnarchiver = aDecoder as! NSKeyedUnarchiver
        
        let decodedName: String = keyedUnarchiver.decodeObjectForKey("MechName") as! String
        let decodedAddress: String = keyedUnarchiver.decodeObjectForKey("MechAddress") as! String
        let decodedContactNo: String = keyedUnarchiver.decodeObjectForKey("MechContactNo") as! String
        self.init(name: decodedName, address: decodedAddress, contactNo: decodedContactNo)
//        self.init(name: keyedUnarchiver.decodeObjectForKey("MechName") as! String)
//        self.address = keyedUnarchiver.decodeObjectForKey("MechAddress") as? String
//        self.contactNo = keyedUnarchiver.decodeObjectForKey("MechContactNo") as? String
        
        if let imageData = keyedUnarchiver.decodeObjectForKey("MechLogo") as? NSData {
            self.logo = UIImage(data: imageData)
        }
    }
    
}