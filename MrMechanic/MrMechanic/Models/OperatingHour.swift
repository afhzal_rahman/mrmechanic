//
//  OperatingHour.swift
//  MrMechanic
//
//  Created by Afhzal Abdul Rahman on 11/18/15.
//  Copyright © 2015 ACME. All rights reserved.
//

import Foundation

class OperatingHour {
    var dayType: Day
    var startTime: String
    var endTime: String
    
    enum Day:String {
        case Weekday
        case Weekend
        case PublicHoliday
        
    }
    
    init(dayType: Day, startTime: String, endTime: String){
        self.dayType = dayType
        self.startTime = startTime
        self.endTime = endTime
    }
    
}