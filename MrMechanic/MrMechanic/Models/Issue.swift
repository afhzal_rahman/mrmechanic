//
//  Issue.swift
//  MrMechanic
//
//  Created by Fadzlil on 11/18/15.
//  Copyright © 2015 ACME. All rights reserved.
//

import Foundation
import CoreLocation

class Issue {
    
    var category: String
    var location: CLLocation
    
    init(category: String, location:CLLocation) {
        self.category = category
        self.location = location
    }
}